## Cereble Flex Starter

#v1.0.3

A starter template for designing websites based on Zurb Foundation flex grids.

Foundation Version 6.4.2

1. Contains foundation flex grid base and visibility classes.

2. Number of columns is set to 24.

3. Max width is set to 1920px or 137.14rem.

Font Awesome Version 5.2.0


#Version History

v1.0.3

1. Updated FontAwesome To Version 5.2.0.

2. Added Foundation Media Queries.